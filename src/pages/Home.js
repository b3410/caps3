import React from 'react';
import Carousel from 'react-bootstrap/Carousel'
import 'pure-react-carousel/dist/react-carousel.es.css';
/*import Banner from './../components/Banner'*/
import Highlights from './../components/Highlights';
import Footer from './../components/Footer';

/*const data = {
	title: 'Welcome to the Home Page',
	content: 'Shop whenever, wherever you want'
};*/
export default function Home() {
	return(
		<div>
		<Carousel>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>Shop Easily</h3>
		      <p>♥♥♥♥♥♥♥♥♥♥</p>
		    </Carousel.Caption>
		    <img 
		      className="d-block w-100"
		      src="https://images.unsplash.com/photo-1544194215-541c2d3561a4?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"
		      alt="First slide"
		    />
		  </Carousel.Item>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>Wherever you want</h3>
		      <p>♥♥♥♥♥♥♥♥♥♥</p>
		    </Carousel.Caption>
		    <img
		      className="d-block w-100"
		      src="https://images.unsplash.com/photo-1517315003714-a071486bd9ea?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80https://place-puppy.com/300x200"
		      alt="Second slide"
		    />

		  </Carousel.Item>
		  <Carousel.Item>
		    <Carousel.Caption>
		      <h3>Whenever you want</h3>
		      <p>♥♥♥♥♥♥♥♥♥♥</p>
		    </Carousel.Caption>
		    <img
		      className="d-block w-100 active"
		      src="https://images.unsplash.com/photo-1551499779-ee50f1aa4d25?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=871&q=80"
		      alt="Third slide"
		    />

		  </Carousel.Item>
		</Carousel>
{/*
			<Banner bannerData={data}/>*/}
			<Highlights />
			<Footer />
		</div>
		);
};